import React, {
  //  useEffect,
  useState
} from 'react'

const App = () => {
  const [input, setInput] = useState('');
  const [itemList, setItemList] = useState([]);

  // useEffect(()=>{ 
  // },[])


  const changeHandle = (event) => {
    setInput(event.target.value)
  }

  const handleSubmit = () => {
    setItemList([...itemList, { item: input, key: Date.now() }])
    // alert(input)
    setInput('')
  }
  const remove = (key) => {
    // console.log(index)
    const newData = itemList.filter(obj => {
      return (obj.key !== key)
    })
    setItemList(newData)
  }


  return (
    <div>
      <div>
        <input value={input} onChange={changeHandle} />
        <button onClick={handleSubmit}>add</button>
      </div>
      <div>
        {itemList.map((val) => {
          return (
            <div key={val.key}>
              {console.log(val)}
              <h3 >{val.item}</h3>
              <button onClick={() => remove(val.key)}>delete</button>
            </div>)
        })}
      </div>
    </div>
  )
}

export default App