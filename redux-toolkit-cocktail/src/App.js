import { Route, Routes } from "react-router-dom";
import "./App.css";
import Layout from "./components/Layout";
import Home from "./pages/Home";
import ProductDetails from "./pages/ProductDetails";
import PageNotFound from "./pages/PageNotFound";
import SearchBox from "./components/SearchBox";
import React from "react";


const App=()=> {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={ 
        <>
         <SearchBox/>
         <Home />
        </>} />
        <Route path="/products/:id" element={ <ProductDetails/> }/>
        <Route path="*" element={<PageNotFound/>} />
      </Routes>
    </Layout>
  );
}

export default App;
