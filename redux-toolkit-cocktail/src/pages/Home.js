import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { fetchCocktails } from "../redux/cocktailSlice";
import Spinner from '../components/Spinner'
import {Link} from 'react-router-dom'
const Home = () => {
  const [modifiedCocktails, setmodifiedCocktails] = useState([]);
  const { loading, cocktails, error } = useSelector(state => ({
    ...state.cocktail
  }));

  const dispatch = useDispatch();
  useEffect(
    () => {
      dispatch(fetchCocktails());
    },
    [dispatch]
  );
  useEffect(() => {
    if (cocktails) {
      const newCocktails = cocktails.map(item => {
        return {
          id: item.idDrink,
          name: item.strDrink,
          img: item.strDrinkThumb,
          info: item.strAlcoholic,
          glass: item.strGlass
        };
      });
      setmodifiedCocktails(newCocktails);
    } else {
      setmodifiedCocktails([]);
    }
  },[cocktails]);

  if(loading){
    return<Spinner/>
  }
  if (error) {
    return <p>{error.message}</p>;
  }
  if (!cocktails) {
    return (
       <h2>No Cocktail Found With THis Name</h2>
       );
  }
  return (
    <>
      <div className="container">
        <div className="row">
          {modifiedCocktails.map((item) => (
            <div className="col-md-2 mt-3 m-1" key={item.id}>
              <div className="card" style={{ width: "12rem" }}>
                <img src={item.img} className="card-img-top" alt={item.name} />
                <div className="card-body">
                  <h5 className="card-title">{item.name}</h5>
                  <h5 className="card-title">{item.glass}</h5>
                  <p className="card-text">{item.info}</p>
                  <Link to={`/products/${item.id}`} className="btn btn-primary">
                    Details
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
};


export default Home;
