// Basic Forms
// http://localhost:3000/isolated/exercise/06.js

import * as React from 'react'

function UsernameForm({ onSubmitUsername }) {
  const val = React.useRef()
  // const [err, setErr] = React.useState(null)
  const [username, serUsername] = React.useState('')
  const handleSubmit = (event) => {
    event.preventDefault()
    console.log(event, 'events')
    console.log('submitted')
    console.log(val.current.value, 'output by useref')
    onSubmitUsername(val.current.value)
    onSubmitUsername(event.target.elements[0].value, 'normal o/p')
    onSubmitUsername(username)
  }
  const handleChange = (event) => {
    //   const value= event.target
    //  const valid= value===value.toLowecase()
    //  setErr(valid?null:'Write in lowecase')
    const { value } = event.target
    // const isLowerCase = value === value.toLowerCase()
    // setErr(isLowerCase ? null : 'Username must be lower case')
    serUsername(value.toLowerCase())
  }

  // 🐨 add a submit event handler here (`handleSubmit`).
  // 💰 Make sure to accept the `event` as an argument and call
  // `event.preventDefault()` to prevent the default behavior of form submit
  // events (which refreshes the page).
  // 📜 https://developer.mozilla.org/en-US/docs/Web/API/Event/preventDefault
  //
  // 🐨 get the value from the username input (using whichever method
  // you prefer from the options mentioned in the instructions)
  // 💰 For example: event.target.elements[0].value
  // 🐨 Call `onSubmitUsername` with the value of the input

  // 🐨 add the onSubmit handler to the <form> below

  // 🐨 make sure to associate the label to the input.
  // to do so, set the value of 'htmlFor' prop of the label to the id of input
  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label>Username:</label>
        <input ref={val} type="text" value={username} onChange={handleChange} />
        {/* <div> {err}</div> */}
      </div>
      <button
      //  disabled={Boolean(err)} 
       type="submit" >Submit</button>
    </form>
  )
}

function App() {
  const onSubmitUsername = username => alert(`You entered: ${username}`)
  return <UsernameForm onSubmitUsername={onSubmitUsername} />
}

export default App
