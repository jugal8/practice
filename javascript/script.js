console.log('hello world..!')




//basics
// console.log('hello world')

// var data = 4
// console.log(data)

// var arr = [2, 4, 6, 8]
// console.log(arr)
// console.log(arr[3])
// console.log(arr.length)

// //object

// var var1 = {
//     name: 'jugal'
// }
// console.log(var1)
// console.log(var1.name)
// console.log(typeof var1)

// //function
// function cal() {
//     console.log('cal function is running')
// }
// cal()
// var cal2 = function () {
//     console.log('cal2 function is running')
// }
// cal2()
// function cal(num1, num2) {
//     return num1 + num2
// }
// cal(12.2, 3)
// console.log(cal(12.2, 4.9))

// //condition

// var condition = false
// var secondCondition = true
// if (condition) {
//     console.log('executing..')
// } else if (secondCondition) {
//     console.log('still executing..')
// } else {
//     console.log('Not executing..')
// }

// //switch
// var no = 3
// switch (no) {
//     case 1: console.log('case1'); break;
//     case 3: console.log('case3'); break;
//     default: console.log('Default')
// }
// //for Loop
// var i
// for (i = 3; i < 9; i++) {
//     console.log(i)
// }
// while
// var x=4
// while(x<9){
//     console.log('while Loop',x);
//     x++
// }
//do-while
// var condition=true
// do{
//     // console.log('executing..')
// }while(condition)


// Day 2

// console.log(Boolean(1));
// console.log(Boolean(0));
// console.log(Boolean("hello"));
// console.log(Boolean(""));

// let str = "123";
// console.log(typeof str);

// let num = Number(str);

// console.log(typeof num);

//single sile comment

/*
..
multiple line comment
...
*/

//Operators
// let x = 1, y = 3;
// console.log(y - x);
// console.log(5 % 2);
// console.log(2 ** 4);
// console.log('1' + 2 + 2);

// let n=2;
// n+=5;
// console.log(n)

// let m=2;
// m=+5;  //not working
// console.log(m)


// var m = 2
// console.log(++m)  //3
// console.log(m++)
// console.log(m)


//bitwise operator
// and, or, xor, not, leftshift, rightshift 
// & , | , ^ , ~ , << , >>


// let a = 2 + 2;

// switch (a) {
//   case 3:
//     console.log( 'Too small' );
//     break;
//   case 4:
//     console.log( 'Exactly!' );
//     break;
//   case 5:
//     console.log( 'Too big' );
//     break;
//   default:
//     console.log( "I don't know such values" );
// }


// console.log(true == 1); // true
// console.log(false == 0); // true
// console.log(0 == false);
// console.log(0 === false);

// //function

// let userName = 'John';

// function showMessage() {
//     let userName = "Bob"; // declare a local variable

//     let message = 'Hello, ' + userName; // Bob
//     console.log(message);
// }

// showMessage();

// console.log(userName);


// const math = Math.pow(3, 2)
// console.log(math);

// //const, var, let
// const myBirthday = '18.04.1982';

// myBirthday = '01.01.2001'; // error,
// console.log(myBirthday)

// let let = 5; // can't name a variable "let", error!
// 
// let return = 5;

// let a = 'val'
// a = 'new value'
// console.log(a)
// var b = 'value'
// var b = 'New Value'
// console.log(b)

// //conditional rendering
// var age = 5
// age > 18 ? console.log('adult') : console.log('kid')

// // data type can be 

// // Number, string, bool, null, undefine, symbol, obj

// let hour = 12;
// let minute = 0;

// if (hour == 12 && minute == 30) {
//     console.log('The time is 12:30');
// } else (console.log('The time is not 12:30'))

// let hours = 9;

// if (hour < 10 || hour > 18) {
//     console.log('The office is closed.');
// }

// console.log(true && true);   // true
// console.log(false && true);  // false
// console.log(true && false);  // false
// console.log(false && false); // false

// //arrow func

// let sum = (a, b) => {
//     let result = a + b;
//     return result;
// };

// console.log(sum(1, 2)); // 3

// //obj
// let user = {     // an object
//     name: "John",
//     age: 30
// }
// console.log(user.name); // John
// console.log(user.age);

// //this

// let users = {
//     name: "Jugal suthar",
//     age: 30,

//     sayHi() {
//       // "this" is the "current object"
//       console.log(this.name);
//     }

//   };

//   users.sayHi();

//   function User(name) {
//     this.name = name;
//     this.isAdmin = false;
//   }

//   let userss = new User("Jack");

//   console.log(userss.name); // Jack
//   console.log(userss.isAdmin); // false

//   let str_ = "Hello";

// console.log( str_.toUpperCase() ); 

// //number
// let billion = 1e9;  // 1 billion, literally: 1 and 9 zeroes
// console.log(billion)
// console.log( 7.3e9 );

// console.log( Math.random(3,10));
// console.log( Math.max(3, 5, -10, 0, 1) ); // 5
// console.log( Math.min(1, 2) );

// function getRndInteger(min, max) {
//     return (Math.random() * (max - min) ) + min;
//   }

// var val =getRndInteger(10,13)
// console.log(val)

// //string
// console.log('\tjugal \vjugal')

// //array
// let fruits = ["Apple", "Orange", "Plum"];

// console.log( fruits[0] ); // Apple
// console.log( fruits[1] ); // Orange
// console.log( fruits[2] ); // Plum
// console.log( fruits[fruits.length-1] );
// console.log( fruits.at(-2) );
// console.log(fruits.pop())
// let shoppingCart = fruits;
// shoppingCart.push("Banana");

// // what's in fruits?
// console.log( fruits.length );
// let matrix = [
//     [1, 2, 3],
//     [4, 5, 6],
//     [7, 8, 9]
//   ];

//   console.log( matrix[1][1] )

// let arr = ["I", "study", "JavaScript"];

// arr.splice(1, 1); // from index 1 remove 1 element

// console.log(arr);

// let arrry = ["I", "study", "JavaScript", "right", "now"];
// arrry.splice(2, 4, "Let's", "dance");

// console.log(arrry)

// //splice, slice, split, join, contact , forEach, indexOf, lastIndexOf, sort, length, filter, map, reverse

// let arr1 = ["t", "e", "s", "t"];

// console.log(arr1.slice(1, 3));

// console.log(arr1.slice(-2));

// let arr2 = [1, 2];

// console.log(arr2.concat([3, 4])); // 1,2,3,4

// console.log(arr2.concat([3, 4], [5, 6]));

// ["Bilbo", "Gandalf", "Nazgul"].forEach((item, index, array) => {
//     console.log(`${item} is at index ${index} in ${array}`);
// });

// let arr3 = [1, 0, false];

// console.log(arr3.indexOf(0)); // 1
// console.log(arr3.indexOf(false)); // 2
// console.log(arr3.indexOf(null)); // -1

// console.log(arr3.includes(1)); // true
// let users = [
//     { id: 1, name: "John" },
//     { id: 2, name: "Pete" },
//     { id: 3, name: "Mary" },
//     { id: 4, name: "John" }
// ];

// // Find the index of the first John
// console.log(users.findIndex(user => user.name == 'mary'));

// let names = 'Bilbo, Gandalf, Nazgul';

// let arr4 = names.split(', ');

// for (let name of arr4) {
//     console.log(`A message to ${name}.`);
// }

// let arr5 = ['Bilbo', 'Gandalf', 'Nazgul'];

// let str = arr5.join(';');

// console.log(str);

// let user = {
//     name: "John",
//     age: 30
// };

// // loop over values
// for (let value of Object.values(user)) {
//     console.log(value); // John, then 30
// }

// //Date

// let now = new Date();
// console.log(now);

// let Jan01_197 = new Date(0);
// console.log(Jan01_197);

// let Dec31_1969 = new Date(-24 * 3600 * 1000);
// console.log( Dec31_1969 );
// let date = new Date("2017-01-26");
// console.log(date);

// let start = new Date(); // start measuring time

// // do the job
// for (let i = 0; i < 100000; i++) {
//   let doSomething = i * i * i;
// }

// let end = new Date(); // end measuring time

// console.log( `The loop took ${end - start} ms` );

//Json

// let student = {
//     name: 'John',
//     age: 30,
//     isAdmin: false,
//     courses: ['html', 'css', 'js'],
//     spouse: null
//   };

//   let json = JSON.stringify(student);

//   console.log(typeof json); // we've got a string!

//   console.log(json);
//   console.log(JSON.parse(json))

//   let room = {
//     number: 23
//   };

//   let meetup = {
//     title: "Conference",
//     participants: [{name: "John"}, {name: "Alice"}],
//     place: room // meetup references room
//   };

//   room.occupiedBy = meetup; // room references meetup

//   console.log( JSON.stringify(meetup, ['title', 'participants']) );

//   try {
//     console.log( 'try' );
//     if (confirm('Make an error?')) BAD_CODE();
//   } catch (err) {
//     console.log( 'catch' );
//   } finally {
//     console.log( 'finally' );
//   }

//   document.cookie = "user=John"; // update only cookie named 'user'
// console.log(document.cookie); 

// special characters (spaces), need encoding
let name = "my name";
let value = "John Smith"

// encodes the cookie as my%20name=John%20Smith
document.cookie = encodeURIComponent(name) + '=' + encodeURIComponent(value);

console.log(document.cookie);

// cookie will die in +1 hour from now
document.cookie = "user=John; max-age=3600";

//Rest parameters
// function sum(a, b) {
//     return a + b;
// }

// console.log(sum(1, 2, 3, 4, 5));

// let arr1 = [1, -2, 3, 4];
// let arr2 = [8, 3, -8, 1];
// console.log(...arr1)
// console.log(arr1)

// console.log(Math.max(...arr1, ...arr2));
// console.log(Math.max(arr1, arr2));

// let arr = [1, 2, 3];

// let arrCopy = [...arr];

// console.log(JSON.stringify(arr) === JSON.stringify(arrCopy)); // true

// console.log(arr === arrCopy); // false (not same reference)

// arr.push(4);
// console.log(arr); // 1, 2, 3, 4
// console.log(arrCopy);

// {
//     let message = "Hello"; // only visible in this block

//     console.log(message); // Hello
// }

// // console.log(message);


// let timerId = setInterval(() => console.log('tick'), 1000);

// // after 5 seconds stop
// setTimeout(() => { clearInterval(timerId); console.log('stop'); }, 5000);

// 
// function printNumbers(from, to) {
//     let current = from;
  
//     let timerId = setInterval(function() {
//       console.log(current);
//       if (current == to) {
//         clearInterval(timerId);
//       }
//       current++;
//     }, 1000);
//   }
  
//   // usage:
//   printNumbers(5, 10);

//   let promise = new Promise(function(resolve, reject) {
//     setTimeout(() => resolve("done!"), 1000);
//   });
  
//   // resolve runs the first function in .then
//   promise.then(
//     result => console.log(result), // shows "done!" after 1 second
//     error => console.log(error) // doesn't run
//   );
///////////////
  // the execution: catch -> catch
new Promise((resolve, reject) => {

    throw new Error("Whoops!");
  
  }).catch(function(error) { // (*)
  
    if (error instanceof URIError) {
      // handle it
    } else {
      console.log("Can't handle such error");
  
      throw error; // throwing this or another error jumps to the next catch
    }
  
  }).then(function() {
    /* doesn't run here */
  }).catch(error => { // (**)
  
    console.log(`The unknown error has occurred: ${error}`);
    // don't return anything => execution goes the normal way
  
  });

  let urls = [
    'https://api.github.com/users/iliakan',
    'https://api.github.com/users/remy',
    'https://no-such-url'
  ];
  
  Promise.allSettled(urls.map(url => fetch(url)))
    .then(results => { // (*)
      results.forEach((result, num) => {
        if (result.status == "fulfilled") {
          console.log(`${urls[num]}: ${result.value.status}`);
        }
        if (result.status == "rejected") {
          console.log(`${urls[num]}: ${result.reason}`);
        }
      });
    });