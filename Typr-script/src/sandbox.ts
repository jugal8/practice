// const named = 'jugal';
// console.log(named)

// let right = true;

// // right='false'
// right = false;

// const fun = (diameter: number) => {
//     return diameter
// }

// console.log(fun(9))

// // array

// let names = ['jugal', 'test2', 'test3'];
// names.push('test4');
// // names.push(20)


// //object
// let obj = {
//     name: 'jugsl',
//     age: 20
// }

// obj.name = 'jubin'
// // obj.name=23

// let mixed: (string | number | boolean)[] = [];
// mixed.push('hello');
// mixed.push(20);
// mixed.push(false);
// console.log(mixed)

// let age: any

// age = 'twenty'
// console.log(age)
// age = 20
// console.log(age)
// age = true
// console.log(age)
// age = { age: '20' }
// console.log(age)

// //function basae

// const add = (a: number, b: number, c: number | string = 10) => {
//     console.log(a + b)
//     console.log(c);
// }
// add(2.01, 23, 'jugal')

// const anchor = document.querySelector('a')!

// console.log(anchor)
// if (anchor) {
//     console.log(anchor.href)
// }
// console.log(anchor.href)
// console.log('ds')

// const form = document.querySelector('.new-item-form') as HTMLFormElement;

// const type = document.querySelector('#type') as HTMLSelectElement;
// const toforom = document.querySelector('#toform') as HTMLInputElement;
// const details = document.querySelector('#details') as HTMLInputElement;
// const amount = document.querySelector('#amount') as HTMLInputElement;

// form.addEventListener('submit', (e: Event) => {
//     e.preventDefault();
//     console.log(
//         type.value,

//     )
// })

// interfaces

interface IsPersion {
    name: string;
    age: number;
}

const me: IsPersion = {
    name: 'jugal',
    age: 20 
}
// const mee: IsPersion = {
//     name: 'jugal',
//     age: "20" 
// }
console.log(me)
// console.log(mee)

interface Point {
    x: number;
    y: number;
  }
   
  function logPoint(p: Point) {
    console.log(`${p.x}, ${p.y}`);
  }
   
  // logs "12, 26"
  const point = { x: 12, y: 26 };
  logPoint(point);