import { Route, Switch } from 'react-router-dom';
import './App.css';
import Nav from './components/Nav';
import About from './pages/About';
import Home from './pages/Home';
import Products from './pages/Products';

function App() {
  return (

    <>
      <Nav />
      <Switch>
        <Route path='/home'>
          <Home />
        </Route>
        <Route path='/about'>
          <About />
        </Route>
        <Route path='/product/:id'>
          <Products />
        </Route>
      </Switch>
    </>

  );
}

export default App;
