import React from 'react'
import { Link } from 'react-router-dom';

const About = () => {
  return (
    <div>About
        <Link to='product/1'>product 1</Link>
        <Link to='product/2'>product 2</Link>
        <Link to='product/3'>product 3</Link>
    </div>
  )
}

export default About