import React from 'react';
import {  NavLink } from 'react-router-dom';
import './nav.css'
const Nav = () => {
    return (
        <div className='nav'>
            <NavLink  activeclassName='link' to='/home'>home</NavLink>
            <NavLink activeclassName='link' to='/about'>about</NavLink>
            
        </div>
    );
};

export default Nav;