import { useState } from "react";
import "./App.css";
import NewTodo from "./component/NewTodo";
import Todos from "./component/Todos";
import Todo from "./models/todo";
const App = () => {
  const [todos, setTodos] = useState<Todo[]>([]);
  const addTodoHandler = (text: string) => {
    const newTodos = new Todo(text);

    setTodos(prevState => {
      return prevState.concat(newTodos);
    });
  };
  const removeTodo = (todoId: string) => {
    setTodos(prevstate => {
      return prevstate.filter(todo => todo.id !== todoId);
    });
  };
  return (
    <div className="main">
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} remove={removeTodo} />
    </div>
  );
};

export default App;
