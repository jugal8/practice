// import React from "react"

import React from "react";

const TodoItem:React.FC <{text:string;RemoveHandler:()=>void}> = ({text,RemoveHandler}) => {

  return (
    <li onClick={RemoveHandler}>{text}  </li>
  )
}

export default TodoItem