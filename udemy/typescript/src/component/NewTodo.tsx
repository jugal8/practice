import React, { useRef } from "react";

const NewTodo:React.FC<{onAddTodo:(text:string)=>void}> = ({onAddTodo}) => {
  const inputref = useRef<HTMLInputElement>(null);
  const submitHandler = (e: React.FormEvent) => {
    e.preventDefault();
    const enteredText = inputref.current!.value;

    if (enteredText.trim().length === 0) {
      return;
    }
    onAddTodo(enteredText);
  };
  return (
    <form onSubmit={submitHandler}>
      <label htmlFor="todo">Todo text</label>
      <input type="text" id="todo" ref={inputref} />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
