import React from "react";
import Todo from "../models/todo";
import TodoItem from "./TodoItem";

const Todos: React.FC<{ items: Todo[]; remove: (id:string) => void }> = ({
  remove,
  items
}) => {
  return (
    <ul>
      {items.map(item =>
        <TodoItem key={item.id} text={item.text} RemoveHandler={remove.bind(null,item.id)} />
      )}
    </ul>
  );
};

export default Todos;
