import { useDispatch, useSelector } from 'react-redux';
import { calculateTotals} from './store/counterSlice'
import { useEffect } from 'react';
import Navbar from './component/Navbar';
import CartContainer from './component/CartContainer';
// import Modal from './components/Modal';
function App() {
  const { cartItems } = useSelector((store) => store.counter);
  // const { isOpen } = useSelector((store) => store.modal);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(calculateTotals());
  }, [cartItems,dispatch]);

  // useEffect(() => {
  //   dispatch(getCartItems('random'));
  // }, []);

  // if (isLoading) {
  //   return (
  //     <div className='loading'>
  //       <h1>Loading...</h1>
  //     </div>
  //   );
  // }

  return (
    <main>
      {/* {isOpen && <Modal />} */}
      <Navbar/>
      <CartContainer/>
    </main>
  );
}
export default App;