import { createSlice } from "@reduxjs/toolkit";
import cartItems from "../cartItems";

const initialState = {
  amount: 0,
  cartItems: cartItems,
  total: 0,
  isLoading: true
};

export const counter = createSlice({
  name: "counter",
  initialState,
  reducers: {
    clearCart: state => {
      state.cartItems = [];
    },
    removeItem: (state, action) => {
      const itemId = action.payload;
      state.cartItems = state.cartItems.filter(item => item.id !== itemId);
    },
    increase2: (state, action) => {
      const id = action.payload;
      const cartItem = state.cartItems.find(item => item.id === id);
      cartItem.amount++;
    },
    // increase: (state, { payload }) => {
    //   const cartItem = state.cartItems.find(item => item.id === payload.id);
    //   cartItem.amount = cartItem.amount + 1;
    // },
    decrease: (state, { payload }) => {
      const cartItem = state.cartItems.find(item => item.id === payload.id);
      cartItem.amount = cartItem.amount - 1;
    },
    calculateTotals: state => {
      let amount = 0;
      let total = 0;
      state.cartItems.forEach(item => {
        amount += item.amount;
        total += item.amount * item.price;
      });
      state.amount = amount;
      state.total = total;
    }
  }
});

export const {
  clearCart,
  removeItem,
  increase,
  decrease,
  calculateTotals,
  increase2
} = counter.actions;
export default counter.reducer;
