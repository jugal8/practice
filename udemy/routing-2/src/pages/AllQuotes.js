import React from 'react'
import QuoteList from '../components/quotes/QuoteList'

const DUMMY_QUOTES =[
  {
    id:'q1',author:'test',text:'description'
  },
  {
    id:'q2',author:'test2',text:'description2'
  },
]

const AllQuotes = () => {
  return (
    <div><QuoteList quotes={DUMMY_QUOTES}/></div>
  )
}

export default AllQuotes