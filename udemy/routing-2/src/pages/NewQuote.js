import React from 'react'
import QuoteForm from '../components/quotes/QuoteForm'
const NewQuote = () => {
  const addHandler =(data)=>{
    console.log(data)
  }
  return (
    <div><QuoteForm onAddQuote={addHandler}/></div>
  )
}

export default NewQuote