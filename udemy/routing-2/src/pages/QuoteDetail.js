import React from 'react';
import { Route, useParams } from 'react-router-dom';
import Comments from '../components/comments/Comments';
import HighlightedQuote from '../components/quotes/HighlightedQuote'

const DUMMY_QUOTES = [
    {
        id: 'q1', author: 'test', text: 'description'
    },
    {
        id: 'q2', author: 'test2', text: 'description2'
    },
];
const QuoteDetail = () => {
    const params = useParams();
    const quote = DUMMY_QUOTES.find(quote=>quote.id===params.quoteId);
   
   if(!quote){
    return <h1> No Quote Found...!</h1>
   }
    return (
        <>
        <HighlightedQuote  text={quote.id} author={quote.author} />
            <Route path={`/quotes/${params.quoteId}/comments`}>
                <Comments />
            </Route>
        </>
    );
};

export default QuoteDetail;