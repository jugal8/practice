import { useState, useRef } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../store/loginSlice";
import classes from "./AuthForm.module.css";
const KEY = process.env.REACT_APP_API_KEY;

const AuthForm = () => {
  const dispatch = useDispatch();
  const emailInputRef = useRef();
  const passwordInputRef = useRef();
  const [isLogin, setIsLogin] = useState(true);
  const [loading, setLoading] = useState(false);
  const switchAuthModeHandler = () => {
    setIsLogin(prevState => !prevState);
  };
  const submit = e => {
    e.preventDefault();
    const enteredEmail = emailInputRef.current.value;
    const enteredpassword = passwordInputRef.current.value;

    setLoading(true);
    if (isLogin) {
      axios
        .post(
          `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${KEY}`,
          {
            email: enteredEmail,
            password: enteredpassword,
            returnSecureToken: true
          }
        )
        .then(setLoading(false))
        .then(res => dispatch(login (res.data.idToken)))
        .catch(err => alert(err.response.data.error.message));
    } else {
      axios
        .post(
          `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${KEY}`,
          {
            email: enteredEmail,
            password: enteredpassword,
            returnSecureToken: true
          }
        )
        .then(setLoading(false))
        .then(res => console.log(res))
        .catch(err => alert(err.response.data.error.message));
    }
  };
  return (
    <section className={classes.auth}>
      <h1>
        {isLogin ? "Login" : "Sign Up"}
      </h1>
      <form onSubmit={submit}>
        <div className={classes.control}>
          <label htmlFor="email">Your Email</label>
          <input type="email" id="email" required ref={emailInputRef} />
        </div>
        <div className={classes.control}>
          <label htmlFor="password">Your Password</label>
          <input
            type="password"
            id="password"
            required
            ref={passwordInputRef}
          />
        </div>
        <div className={classes.actions}>
          {!loading &&
            <button onClick={submit}>
              {isLogin ? "Login" : "Create Account"}
            </button>}
          {loading && <h3>Loading...</h3>}

          <button
            type="button"
            className={classes.toggle}
            onClick={switchAuthModeHandler}
          >
            {isLogin ? "Create new account" : "Login with existing account"}
          </button>
        </div>
      </form>
    </section>
  );
};

export default AuthForm;
