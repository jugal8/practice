import { createSlice } from "@reduxjs/toolkit";
// import React,{useState} from 'react'
const initialState = {
  token: "",
  isLoggedIn: false,
  login: token => {},
  logout: () => {}
};
// const[token,setToken]= useState('') 


const loginSlice = createSlice({
  name: "login",
  initialState,
  reducers: {
    login: (token) => {
      return token;
    },
    isLoggedOut:()=>{
        return initialState.token
    }
  }
});

export const {login,isLoggedOut} = loginSlice.actions
export default loginSlice.reducer;
