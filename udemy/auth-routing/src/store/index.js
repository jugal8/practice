import { configureStore } from "@reduxjs/toolkit";
import LoginSlice from "./loginSlice.js";

export const store = configureStore({
  reducer: {
    auth: LoginSlice,
  },
});
