import ProductItem from "./ProductItem";
import classes from "./Products.module.css";
const DUMMY_DATA = [
  {
    id: "p1",
    price: 5,
    title: "pen",
    descriptions: "this is good pen"
  },
  {
    id: "p2",
    price: 45,
    title: "i-phone",
    descriptions: "need lot's of money to buy "
  }
];

const Products = props => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_DATA.map(product =>
          <ProductItem
            key={product.id}
            id={product.id}
            title={product.title}
            price={product.price}
            description={product.descriptions}
          />
        )}
      </ul>
    </section>
  );
};

export default Products;
