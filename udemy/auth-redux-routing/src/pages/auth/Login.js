import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import React, { useState } from "react";
// import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
//   const user = useSelector(state => state.auth.value);
  const auth = getAuth();
  const navigate=useNavigate()
  const signIn = () => {

    setLoading(true);

    signInWithEmailAndPassword(auth, email, password)
      .then(userCredential => {
        const user = userCredential.user;
        
        setLoading(false);
        navigate('/home')
        console.log("Singed in user: ", user);
        localStorage.setItem('user',user.stsTokenManager.refreshToken)
      })
      .catch(error => {
        setLoading(false);
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log("An error occured: ", errorCode, errorMessage);
      });
  };
  //   };
  return (<>
  
  {/* {!user &&  */}
    <div>
    <h1>Login</h1>
    Email:
    <br />
    <input
        type="text"
        value={email}
        onChange={e => setEmail(e.target.value)}
    />
    <br />
    Password:
    <br />
    <input
        type="password"
        value={password}
        onChange={e => setPassword(e.target.value)}
    />
    <br />
    <button onClick={signIn}>Log In</button>
    </div>
   {/* }  */}
   {loading && <h1>loading...</h1>}</> );
};

export default Login;
