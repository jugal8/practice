import { getAuth, signOut } from "firebase/auth";
import React from "react";
import { Link, useNavigate } from "react-router-dom";

const Dashbord = () => {
  
  const navigate=useNavigate()
  const auth = getAuth();

  return (
    <div>
      Dashbord
      <h1>Home Page</h1>
      {console.log('dashbord')}
      <p>
        <Link
          to=""
          onClick={() => {
            signOut(auth)
              .then(() => {
                console.log("user signed out");
                navigate('/login')
                localStorage.clear()
                
              })
              .catch(error => {
                console.log("error", error);
              });
          }}
        >
          Log out
        </Link>
      </p>
    </div>
  );
};

export default Dashbord;
