// import React from "react";
// import { Routes, Route, useNavigate } from "react-router";
// import { Navigate } from "react-router-dom";
// import { useSelector } from "react-redux";

// const ProtectedRoute = ({ element: Component }) => {
//   // const id = localStorage.getItem("user");
//   // console.log("id:", id);

//   const navigate = useNavigate();
//   const user = useSelector(state => state.auth.value);
//   console.log("user", user);
//   return (
//     <Routes>
//       <Route
//         element={props => {
//           user ? <Component {...props} /> : navigate("/login");
//         }}
//       />
//     </Routes>
//   );
// };

// export default ProtectedRoute;

import { useSelector } from "react-redux";
import { Navigate } from "react-router-dom";

const ProtectedRoute = ({ children }) => {

  const user = useSelector(state => state.auth.value);
  if (user) {
    return children;
  }
  return <Navigate to="/login" replace />;
};
export default ProtectedRoute;
