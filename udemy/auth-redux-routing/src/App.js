import "./App.css";

import { BrowserRouter, Link, Route, Routes } from "react-router-dom";
import Login from "./pages/auth/Login";
import Register from "./pages/auth/Register";
import Dashbord from "./pages/protected/Dashbord";
import Reset from "./pages/auth/Reset";
import { initializeApp } from "firebase/app";
import { firebaseConfig } from "./configs/firebaseConfig";
import { getAuth, onAuthStateChanged, signOut } from "firebase/auth";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { saveUser } from "./store/authSlice";
// import ProtectedRoute from "./utils/ProtectedRoute";

function App() {
  initializeApp(firebaseConfig);
  // const id = localStorage.getItem("user");
  const auth = getAuth();
  const user = useSelector(state => state.auth.value);
  console.log("user from state", user);
  const dispatch = useDispatch();
  useEffect(
    () => {
      onAuthStateChanged(auth, user => {
        if (user) {
          dispatch(saveUser(user.refreshToken));
        } else {
          dispatch(saveUser(undefined));
        }
      });
    },
    [auth, dispatch]
  );
  console.log(user);
  return (
    <div className="App">
      <BrowserRouter>
        {user && <Link to="/home">home</Link>}

        {!user && <Link to="/login">Login</Link>}

        {!user && <Link to="/register">register</Link>}
        <Link
          to=""
          onClick={() => {
            signOut(auth)
              .then(() => {
                console.log("user signed out");
                // navigate('/login')
                localStorage.clear();
              })
              .catch(error => {
                console.log("error", error);
              });
          }}
        >
          Log out
        </Link>
        <Link to="/reset">reset</Link>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/reset" element={<Reset />} />
          {/* <Route path="/home" element={<Dashbord />} /> */}
          {/* <Route element={<ProtectedRoute />}> */}
          {/* <ProtectedRoute> */}
            {localStorage.getItem("user") && <Route path="/home" element={<Dashbord />} />}
          {/* </ProtectedRoute> */}
          {/* </Route> */}
        </Routes>
        {/* <ProtectedRoute  path="/home" element={<Dashbord />} /> */}
      </BrowserRouter>
    </div>
  );
}

export default App;

//            {(!user || !id) && <Route path="/login" element={<Login />} />}
//           if(!user || !id){<Route path="/login" element={<Login />} />
//           {!user && <Route path="/register" element={<Register />} />}
//           {id && <Route path="/dashbord" element={<Dashbord />} />}
//           <Route path="/Reset" element={<Reset />} />
//           <ProtectedRoute exact path="/protected" component={Dashbord} />
//           <Route path="*" element={<p>page not found</p>} />
