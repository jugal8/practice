import { Route, Routes } from "react-router";
import "./App.css";
import Layout from "./components/Layout";
import SearchBox from "./components/SearchBox";
import Home from "./pages/Home";
import PageNoteFound from "./pages/PageNoteFound";
import ProductDetails from "./pages/ProductDetails";

const App = () => {
  return (
    <Layout>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <SearchBox />
              <Home />
            </div>
          }
        />
        <Route path="/products/:id" element={<ProductDetails />} />
        <Route path="*" element={<PageNoteFound />} />
      </Routes>
    </Layout>
  );
};

export default App;
