import React, { useRef } from "react";
import { fetchSearchCocktails } from "../redux/cocktailSlice";
import { useAppDispatch } from "../features";

const SearchBox = () => {

// const searchTerm = useRef<HTMLInputElement>(null);
const searchTerm =useRef() as React.MutableRefObject<HTMLInputElement>;

const dispatch = useAppDispatch();


  const handleChange = () => {
    const searchText = searchTerm.current?.value;
    if (searchTerm){
      dispatch(fetchSearchCocktails( searchText ));
}
    console.log(searchText)
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
  };
  return (
    <>
      <div className="d-flex flex-column align-items-center justify-content-center mt-4">
        <form onSubmit={handleSubmit}>
          <div className="mb-3">
            <input
              type="email"
              ref={searchTerm}
              onChange={handleChange}
              className="form-control"
              placeholder="Serach Here"
              style={{ width: "350px" }}
            />
          </div>
        </form>
      </div>
    </>
  );
};

export default SearchBox;