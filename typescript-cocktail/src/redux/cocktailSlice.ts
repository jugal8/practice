import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";

type cocktailRes = {
  idDrink?: string;
  strDrink?: string;
  strDrinkThumb?: string;
  strAlcoholic?: string;
  strGlass?: string;
  strCategory?: string;
};
type InitialState = {
  loading: boolean;
  error: null | string;
  cocktails: Array<any>;
  cocktail: cocktailRes;
};

export const fetchCocktails = createAsyncThunk(
  "cocktail/fetchCocktails",
  async () => {
    return fetch("https://www.thecocktaildb.com/api/json/v1/1/search.php?s=")
      .then((res) => res.json())
      .catch((err) => console.log(err));
  }
);

export const fetchSingleCocktails = createAsyncThunk(
  "cocktails/fetchSignleCocktails",
  async (id: string) => {
    return fetch(
      `https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=${id}`
    )
      .then((res) => res.json())
      .catch((err) => console.log("single cocktail erroor", err));
  }
);
export const fetchSearchCocktails = createAsyncThunk(
  "cocktails/fetchSearchCocktails",
  async (searchText: string) => {
    return fetch(
      `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${searchText}`
    )
      .then((res) => res.json())
      .catch((err) => console.log("Search Error", err));
  }
);

const initialState: InitialState = {
  loading: false,
  cocktails: [],
  error: null,
  cocktail: {}
};

const cocktailSlice = createSlice({
  name: "cocktail",
  initialState,
  reducers: {},
  extraReducers(builder) {
    builder
      .addCase(fetchCocktails.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(
        fetchCocktails.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.loading = false;
          state.cocktails = action.payload.drinks;
        }
      )
      .addCase(fetchCocktails.rejected, (state, action: PayloadAction<any>) => {
        state.loading = false;
        state.error = action.payload;
      });
    builder
      .addCase(fetchSingleCocktails.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(
        fetchSingleCocktails.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.loading = false;
          state.cocktail = action.payload.drinks[0];
        }
      )
      .addCase(
        fetchSingleCocktails.rejected,
        (state, action: PayloadAction<any>) => {
          state.loading = false;
          state.error = action.payload;
        }
      );
    builder
      .addCase(fetchSearchCocktails.pending, (state, action) => {
        state.loading = true;
      })
      .addCase(
        fetchSearchCocktails.fulfilled,
        (state, action: PayloadAction<any>) => {
          state.loading = false;
          state.cocktails = action.payload.drinks;
        }
      )
      .addCase(
        fetchSearchCocktails.rejected,
        (state, action: PayloadAction<any>) => {
          state.loading = false;
          state.error = action.payload;
        }
      );
  }
});

export default cocktailSlice.reducer;
