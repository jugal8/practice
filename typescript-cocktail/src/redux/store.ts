import { configureStore } from "@reduxjs/toolkit";
import cocktailSlice from "./cocktailSlice";

const store = configureStore({
  reducer: { cocktail: cocktailSlice }
});
export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
