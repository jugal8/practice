import { useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { fetchSingleCocktails } from "../redux/cocktailSlice";
import { useAppDispatch, useAppSelector } from "../features";

import SpinnerAnim from "../components/Spinner";

const ProductDetails = () => {
  const { loading, cocktail } = useAppSelector(state => ({
    ...state.cocktail
  }));
  const dispatch = useAppDispatch();
  const { id } = useParams();
console.log(cocktail)
  useEffect(
    () => {
      if (id) {
        dispatch(fetchSingleCocktails(id));
      }
    },
    [dispatch, id]
  );

  if (!cocktail) {
    return <h2>No Cocktail Details</h2>;
  } else {
    return (
      <div>
        {loading
          ? <SpinnerAnim />
          : <div className="container mt-4">
              <Link to="/" className="btn btn-info">
                GO BACK
              </Link>
              <div className="row mt-4">
                <div className="col-md-5">
                  <img
                    src={cocktail.strDrinkThumb}
                    alt={cocktail.strDrink}
                    height={300}
                    width={400}
                  />
                </div>
                <div className="col-md-5">
                  <h2>
                    Name : {cocktail.strDrink}
                  </h2>
                  <p className="mt-1">
                    Category : {cocktail.strCategory}
                  </p>
                  <p>
                    Info : {cocktail.strAlcoholic}
                  </p>
                  <p>
                    Glass : {cocktail.strGlass}
                  </p>
                </div>
              </div>
            </div>}
      </div>
    );
  }
};

export default ProductDetails;
