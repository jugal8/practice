import React, { useEffect, useState } from "react";
import { fetchCocktails } from "../redux/cocktailSlice";
import Spinner from "../components/Spinner";
import { Link } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../features";

const Home: React.FC = () => {
  const [modifiedCocktails, setmodifiedCocktails] = useState<
    {
      id: string;
      name: string;
      img: string;
      info: string;
      glass: string;
    }[]
  >([]);
  const dispatch = useAppDispatch();
  const { cocktails, error, loading } = useAppSelector(state => ({
    ...state.cocktail
  }));

  useEffect(
    () => {
      dispatch(fetchCocktails());
    },
    [dispatch]
  );

  useEffect(
    () => {
      if (cocktails) {
        const newCocktails = cocktails.map(item => {
          return {
            id: item.idDrink,
            name: item.strDrink,
            img: item.strDrinkThumb,
            info: item.strAlcoholic,
            glass: item.strGlass
          };
        });
        setmodifiedCocktails(newCocktails);
      } else {
        setmodifiedCocktails([]);
      }
    },
    [cocktails]
  );

  if (loading) {
    return <Spinner />;
  }
  if (error) {
    return <p>Oops..Somthing Went Wrong....!</p>;
  }
  if (!modifiedCocktails) {
    return <h2>No Cocktail Found With THis Name</h2>;
  }
  return (
    <div className="container">
      <div className="row">
        {modifiedCocktails.map(item =>
          <div className="col-md-2 mt-3 m-1" key={item.id}>
            <div className="card" style={{ width: "12rem" }}>
              <img src={item.img} className="card-img-top" alt={item.name} />
              <div className="card-body">
                <h5 className="card-title">
                  {item.name}
                </h5>
                <h5 className="card-title">
                  {item.info}
                </h5>
                <p className="card-text">
                  {item.glass}
                </p>
                <Link to={`/products/${item.id}`} className="btn btn-primary">
                  Details
                </Link>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Home;
