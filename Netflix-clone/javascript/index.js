
console.log('JS file is working ')
var tabItems = document.querySelectorAll('.tab-item');
const tabItem = document.querySelectorAll('div.tab-item');
console.log(tabItems)
const tabContentItems = document.querySelectorAll('.tab-content-item');
// console.log(tabContentItems)
// document.querySelectorAll('.tab-item').forEach(item => {
//     item.addEventListener('click', selectItem);
//     console.log('addEventListener')
// });
tabItems.forEach(item => {
    item.addEventListener('click', selectItem);
    console.log('addEventListener')
});


function selectItem(e) {

    removeBorder();
    removeShow();

    this.classList.add('tab-border');
    console.log(this.id,'this is the id')

    const tabContentItem = document.querySelector(`#${this.id}-content`);

    tabContentItem.classList.add('show');
}

function removeBorder() {
    tabItems.forEach(item => {
        item.classList.remove('tab-border');
    });
}

function removeShow() {
    tabContentItems.forEach(item => {
        item.classList.remove('show');
    });
}
